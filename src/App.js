import React, { useState } from "react";
import { Navbar, Container } from "react-bootstrap";
import  Calendar from "./components/calendar/Calendar";
import  Reminder from "./components/modals/Reminder";
import { deleteReminders } from "./actions/reminder";
import { connect } from "react-redux";

function App(props) {
  const [show, setShow] = useState(false);
  const [editReminder, setEditReminder] = useState({});

  const openReminder = (reminder) => {
    setEditReminder(reminder);
    // here you get the reminder to load it on the dialog
    setShow(true);
  }

  const newReminder = () => {
    setEditReminder({});
    setShow(true);
  }

  const delReminders = (date) => {
    const { deleteReminders } = props;
    deleteReminders(date);
  }
    
  return (
    <>
     <Navbar expand="lg" variant="light" bg="light" fixed="top">
        <Navbar.Brand href="#">
          <img
            src="/jobsity-logo.png"
            width="130"
            height="35"
            className="d-inline-block align-top"
            alt="jobsity"
          />
        </Navbar.Brand>
      </Navbar>
      <div className="calendar-content">
          <Container>
              <div className="main-info">
                  <span>*Jobsity calendar (Miguel Rodríguez)* if you want to add a new reminder use the add button</span>
                  <button onClick={newReminder} className="btn btn-primary">Add</button>
              </div>
          </Container>
          <Calendar eraseReminders={delReminders} openReminder={openReminder}></Calendar>
          <Reminder reminder={editReminder} show={show} handleClose={()=> setShow(false)}></Reminder>
      </div>
    </>
  );
}

export default connect(null, {
  deleteReminders
})(App);
