import {takeLatest, put, select} from 'redux-saga/effects';
import {writeToLocalStorage} from '../utils/persistance';
import {UPSERT_REMINDER, DELETE_REMINDERS, upsertReminderSuccess} from '../actions/reminder';

function* upsertReminderHandler({prevKey, dateKey, payload}){
  const currState = yield select(state=>state.reminder.data);
  if(prevKey !== dateKey){ 
    currState[prevKey] = undefined;
  }
  if (currState[dateKey]) {
    const index = currState[dateKey].findIndex(reminder => payload.uid === reminder.uid);
    if(index !== -1) {
      currState[dateKey][index] = payload;
    } else {
      currState[dateKey].push(payload)
    }
  } else {
    currState[dateKey] = [payload];
  }
  writeToLocalStorage(currState);
  yield put(upsertReminderSuccess(currState));
}

export function* upsertReminderSaga() {
  yield takeLatest(UPSERT_REMINDER, upsertReminderHandler);
}

function* deleteRemindersHandler({dateKey}){
  const currState = yield select(state=>state.reminder.data);
  currState[dateKey] = undefined;
  writeToLocalStorage(currState);
  yield put(upsertReminderSuccess(currState));
}

export function* deleteRemindersSaga() {
  yield takeLatest(DELETE_REMINDERS, deleteRemindersHandler);
}