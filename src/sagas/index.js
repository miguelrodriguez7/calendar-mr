import { fork, all } from 'redux-saga/effects';
import {upsertReminderSaga, deleteRemindersSaga} from './reminder';


export default function* rootSaga() {
  yield all([
    upsertReminderSaga,
    deleteRemindersSaga
  ].map(fork))
}