import {createStore, compose, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {createLogger} from 'redux-logger';
import rootReducer from './reducers';
import rootSaga from './sagas';

const sagasMiddleware = createSagaMiddleware();
const logger = createLogger();

function newStore(initialState={}){
  const middlewares = [sagasMiddleware, logger];
  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middlewares))
  );
  
  sagasMiddleware.run(rootSaga);
  return store
}

export default newStore;