import {UPSERT_REMINDER_SUCCESS} from '../actions/reminder';

const INITIAL_STATE = {
  data: {},
  error: {}
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case UPSERT_REMINDER_SUCCESS:
      const { payload } = action;
      return {...state, data: {...payload}}
    default: return state;
  }
}