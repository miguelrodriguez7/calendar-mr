
const ITEM_ID = 'JOBSITY-CALENDAR-DATA';


export function readFromLocalStorage() {
  try{ 
    const raw = window.localStorage.getItem(ITEM_ID);
    return JSON.parse(raw);
  } catch(e) {
    console.log(`${this.constructor.name}: error has ocurred reading from localstorage`);
  }
}

export function writeToLocalStorage(newData) {
  try{ 
    const serializedData = JSON.stringify(newData);
    window.localStorage.setItem(ITEM_ID, serializedData);
  } catch(e) {
    console.log(`${this.constructor.name}: error has ocurred writing to localstorage`);
  }
}