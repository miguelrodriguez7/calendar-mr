import React from "react";
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Reminder from '../../components/modals/Reminder';
import { Provider } from 'react-redux';
import newStore from '../../store';

Enzyme.configure({adapter: new Adapter()});

describe('Reminder', () => {
  let wrapper;
  let store = newStore();
  const mockClose = jest.fn();
  const mockReminder = {
    uid: "TestUID",
    title: "Test Reminder",
    selectedColor: "red",
    selectedDate: "07/07/2077",
    selectedTime: "10:00",
    selectedState: "London",
  };
 
  beforeAll(() => {
    wrapper = mount(
      <Provider store={store}>
    <Reminder handleClose={mockClose} show={true} reminder={mockReminder}/>
    </Provider>
    );
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should render with given state from Redux store', () => {
    let reminderComponent = wrapper.find('Reminder');
    expect(toJson(reminderComponent)).toMatchSnapshot()
  });

  // Unit test the functionality: ​Ability to add a new "reminder" (max 30 chars) for a user entered day and time. Also, include a city
  it('it save was called', () => {
    let reminderComponent = wrapper.find('Reminder');
    //save funtion mock
    const mockSave = jest.fn();
    reminderComponent.instance().saveChanges = mockSave;
    const clickSpy = jest.spyOn(reminderComponent.instance(), 'saveChanges');
    wrapper.update();
    wrapper.find('button#btn-save').props().onClick();
    reminderComponent.find('button#btn-save').simulate('click');
    expect(clickSpy).toHaveBeenCalled();
  });
});