import React from 'react';
import moment from "moment";
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Calendar from '../../../components/Calendar/Calendar';
import { Provider } from 'react-redux';
import newStore from '../../../store'

Enzyme.configure({adapter: new Adapter()});
const store = newStore();

describe('Calendar', () => {
  let wrapper;
 
  beforeAll(() => {
    wrapper = mount(
      <Provider store={store}>
    <Calendar/>
    </Provider>
    );
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('it renders current month', () => {
    //get current month name
    let monthName = moment().format("MMMM");
    const section = wrapper.find('#month-title');
    expect(section.text().indexOf(monthName) > -1).toBe(true);
  });

  it('it renders current year', () => {
    //get current year
    let year = moment().format("YYYY");
    const section = wrapper.find('#month-title');
    expect(section.text().indexOf(year) > -1).toBe(true);
  });

  it('it renders all month days', () => {
    //get current month name
    let daysInMonth = moment().daysInMonth();
    const days = wrapper.find('div.calendar-day');
    const noCurrent = wrapper.find('div.calendar-no-current-month');
    //total days displayed has to be the same as no current days plus days in month
    expect(days).toHaveLength(daysInMonth + noCurrent.length);
  });
});