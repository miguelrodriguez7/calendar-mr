import React from "react";
import "./Reminder.css";
import { Modal, Button, FormGroup, FormLabel } from "react-bootstrap";
import moment from "moment";
import { upsertReminder } from "../../actions/reminder";
import { connect } from "react-redux";
import axios from "axios";

const colors = ["red", "orange", "blue", "pink", "yellow"];

const states = ["London"];

const wheaterKey = "b6907d289e10d714a6e88b30761fae22";

class Reminder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        uid: "",
        title: "",
        selectedColor: "red",
        selectedDate: "",
        selectedTime: "",
        selectedState: "London",
      },
      elementKey:'',
      error: "",
      weather: ""
    };
  }

  componentDidUpdate(prevProps) {
    let weather = "";
    let current = this;
    if (this.props.reminder !== prevProps.reminder) {
      if (this.props.selectedState || "London") {
        axios
          .get(
            `https://cors-anywhere.herokuapp.com/https://samples.openweathermap.org/data/2.5/weather?q=${this
              .props.selectedState || "London"}&appid=${wheaterKey}`
          )
          .then(function(response) {
            weather = response.data.weather[0];

            current.setState({
              elementKey : (current.props.reminder.selectedDate) 
              ? moment(current.props.reminder.selectedDate).format("MM/DD/YYYY")
              : '',
              form: {
                ...current.props.reminder,
                selectedColor: current.props.reminder.selectedColor
                  ? current.props.reminder.selectedColor
                  : "red",
                selectedState: current.props.reminder.selectedState
                  ? current.props.reminder.selectedState
                  : "London"
              },
              weather: weather
            });
          })
          .catch(function(error) {
            // handle error
            console.log('Error trying to get weather')
          });
      }
    }
  }

  handleInputChange(event) {
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value
      }
    });
  }

  saveChanges = () => {
    this.setState({ error: "" });
    const { selectedDate, selectedTime, title } = this.state.form;
    const { upsertReminder } = this.props;
    if (!selectedDate || !selectedTime || !title) {
      this.setState({
        error:
          "Please check your selected date and time and make sure you added a title"
      });
      return;
    }

    let uid;
    if (this.state.form.uid) {
      uid = this.state.form.uid;
      
    } else {
      uid = Date.now();
    }
    const payload = { ...this.state.form, uid };
    const dateKey = moment(selectedDate).format("MM/DD/YYYY");
    const prevKey = this.state.elementKey ? this.state.elementKey : dateKey;
    upsertReminder(prevKey, dateKey, payload);
    this.props.handleClose();
  };

  render() {
    const { error, form } = this.state;
    return (
      <>
        <Modal show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header>
            <Modal.Title>Reminder</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {error && <div className="alert alert-danger">{error}</div>}
            <FormGroup>
              <FormLabel>Wheater for {form.selectedState} 
              <img alt="" 
              src={this.state.weather 
                ? `http://openweathermap.org/img/wn/${this.state.weather.icon}.png`
                : ''}></img>
              </FormLabel>
            </FormGroup>
            <FormGroup>
              <FormLabel>{ this.state.weather
              ? `${this.state.weather.main} ${this.state.weather.description}`
              : ''
              }</FormLabel>
            </FormGroup>
            <FormGroup>
              <FormLabel>Description</FormLabel>
              <input
                name="title"
                type="text"
                value={form.title || ""}
                onChange={this.handleInputChange.bind(this)}
                maxLength="30"
                className="form-control"
              />
            </FormGroup>
            <FormGroup>
              <FormLabel>Date</FormLabel>
              <input
                type="date"
                className="form-control"
                name="selectedDate"
                value={form.selectedDate || ""}
                onChange={this.handleInputChange.bind(this)}
              />
            </FormGroup>
            <FormGroup>
              <FormLabel>Time (Follow 24 hours format)</FormLabel>
              <input
                type="text"
                maxLength="5"
                name="selectedTime"
                value={form.selectedTime || ""}
                onChange={this.handleInputChange.bind(this)}
                placeholder="00:00"
                className="form-control"
              />
            </FormGroup>
            <FormGroup>
              <FormLabel>City</FormLabel>
              <select
                className="form-control"
                name="selectedState"
                defaultValue={form.selectedState || "London"}
              >
                {states.map((state, i) => {
                  return (
                    <option key={state} value={state}>
                      {state}
                    </option>
                  );
                })}
              </select>
            </FormGroup>
            <FormGroup>
              <FormLabel>Color</FormLabel>
              <select
                className="form-control"
                name="selectedColor"
                defaultValue={form.selectedColor || "red"}
                onChange={this.handleInputChange.bind(this)}
              >
                {colors.map((color, i) => {
                  return (
                    <option key={color} value={color}>
                      {color}
                    </option>
                  );
                })}
              </select>
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.handleClose}>
              Close
            </Button>
            <Button variant="primary" id="btn-save" onClick={this.saveChanges}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default connect(null, {
  upsertReminder
})(Reminder);
