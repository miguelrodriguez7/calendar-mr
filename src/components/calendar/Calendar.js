import React, { useState, useEffect } from "react";
import "./Calendar.css";
import { Row, Col, Container } from "react-bootstrap";
import moment from "moment";
import { faChevronLeft, faChevronRight, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {useSelector, shallowEqual} from 'react-redux';

const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];

function Calendar(props) {
  const reminderState = useSelector(state => state.reminder, shallowEqual);
  const [currentMonth] = useState(moment().format("MM"));
  const [currentMonthName] = useState(moment().format("MMMM"));

  const[currentDay] = useState(moment().format('DD'));
  const [calendarDays, setCalendarDays] = useState([]);
  const [indexDayStart, setIndexDayStart] = useState(0);
  const [indexDayEnd, setIndexDayEnd] = useState(0);

  useEffect(() => {
    let totalDays = [];
    const firstDay = moment()
      .startOf("month")
      .format("dddd");
    const lastDay = moment()
      .endOf("month")
      .format("DD");
      const lastDayName = moment()
      .endOf("month")
      .format("dddd");

    if (firstDay !== "Sunday") {
      const lastDayPastMonth = moment()
        .startOf("month")
        .date(0)
        .format("DD");
      let startDayLastWeek = moment()
        .startOf("month")
        .date(-1)
        .startOf("week")
        .format("DD");

        setIndexDayStart(parseInt(lastDayPastMonth) + 1 - startDayLastWeek);

      while (startDayLastWeek <= lastDayPastMonth) {
        let month = currentMonth !== parseInt(1) ? currentMonth-1 : 12;
        const dateInfo = (moment(new Date(`${month}/${startDayLastWeek}/2020`)).format('MM/DD/YYYY'));
        let remidersData =
        reminderState.data[dateInfo];
        totalDays.push({day:startDayLastWeek, reminders: (remidersData) ? remidersData : []});
        startDayLastWeek++;
      }
    }
    else{
      setIndexDayStart(0);
    }

    let currentMonthDays = [];
    
    for(let i=1; i<=lastDay; i++){
      let month = currentMonth;
      const dateInfo = (moment(new Date(`${month}/${i}/2020`)).format('MM/DD/YYYY'));
      let remidersData =
      reminderState.data[dateInfo];
      totalDays.push({day:i, reminders: (remidersData) ? remidersData : []});
    }

    totalDays = totalDays.concat(
     currentMonthDays
    );

    setIndexDayEnd(parseInt(totalDays.length)-1);

    if(lastDayName !== 'Saturday'){
    let nextSaturday = moment()
        .startOf("month").add(1, 'months')
        .date(1)
        .endOf("week")
        .format("DD");

       for(let newDay = 1; newDay<= parseInt(nextSaturday); newDay++) {
        let month = currentMonth;
        const dateInfo = (moment(new Date(`${month}/${newDay}/2020`)).format('MM/DD/YYYY'));
        let remidersData =
        reminderState.data[dateInfo];
         totalDays.push({day:newDay, reminders: (remidersData) ? remidersData : []});
       } 
    }

    setCalendarDays(totalDays);
  }, [reminderState]);

  return (
    <>
      {/* {JSON.stringify(reminderState, null, '\t')} */}
      <Container>
        <Row>
        <Col sm={1}  className="left-caret">
          <FontAwesomeIcon icon={faChevronLeft} size="2x" />
          </Col>
          <Col sm={10}>
          <h1 id="month-title" className="title text-center"> {currentMonthName} 2020 </h1>
          </Col>
          <Col sm={1} className="right-caret">
          <FontAwesomeIcon icon={faChevronRight} size="2x" />
          </Col>
        </Row>
        <div className="calendar" data-toggle="calendar">
          <Row>
            {days.map((dayName, i) => {
              return (
                <Col key={dayName} className="calendar-header">
                  <span>{dayName}</span>
                </Col>
              );
            })}
          </Row>
          { 
            new Array(calendarDays.length/7).fill(null).map((number, i) => {
              return <Row key={'row'+i}>
              {
                calendarDays.slice(i*7,(i+1)*7).map((d, index) => {
                  return <Col key={'day'+ ((i*7) + index)} className={ 
                  (index + (i*7) < indexDayStart || index + (i*7) > indexDayEnd) 
                  ? "calendar-day calendar-no-current-month" : 
                  // I add this to highlight curret day
                  (currentMonthName === moment().format("MMMM") && parseInt(currentDay) === parseInt(d.day)  )
                   ? "calendar-day current-day"
                  :"calendar-day" }>
                  <time>{d.day}</time>
                  <span style={{float:"right"}}>
                    {
                       d.reminders.length > 0 &&
                      <FontAwesomeIcon onClick={()=> props.eraseReminders(
                        moment(d.reminders[0].selectedDate).format("MM/DD/YYYY"))} className="trash" icon={faTrash} />
                    }
                    </span>
                  <div className="reminders">
                  {   
                    d.reminders.sort((a, b) => a.selectedTime.localeCompare(b.selectedTime) || a.selectedTime.localeCompare(b.selectedTime)).map((e, eventIndex) => {
                    return <div key={eventIndex+'reminder' + ((i*7) + index)} className="reminder"
                    onClick={()=> props.openReminder(e)}>
                    <h4>{e.title}</h4>
                        <div className="progress" style={{backgroundColor:e.selectedColor}}>
                      </div>
                    </div>
                    })
                  }
                  </div>
                  </Col>
                })
              }
              </Row>
            })
          }
        </div>
      </Container>
    </>
  );
}

export default Calendar;
