export const UPSERT_REMINDER = 'UPSERT_REMINDER';
export const UPSERT_REMINDER_SUCCESS = 'UPSERT_REMINDER_SUCCESS';
export const DELETE_REMINDERS = 'DELETE_REMINDERS';


export const upsertReminder = (prevKey, dateKey, payload) => ({
    type: UPSERT_REMINDER,
    payload,
    dateKey,
    prevKey
});

export const deleteReminders = (dateKey) => ({
  type: DELETE_REMINDERS,
  dateKey
});

export const upsertReminderSuccess = (payload) => ({
  type: UPSERT_REMINDER_SUCCESS,
  payload,
});
