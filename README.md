This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
JOBSITY

## Instructions
-Ensure you have internet connection
-You need to run npm i
-Run npm start to run the project

## Important Info

*Code quality can be improve it but I don't have enough time to do it maybe using propTypes or typescript,
sass instead css, linter.
*Calendar is saving data into localStorage, you can remove them by day, but if you want to clear all
you have to do it manually erasing this info from browser.
*React hooks and classes were used to show that i can work with both.
*Id's needs to added if automation were needed.
*Boostrap was used, it has browser compatibility.
*Moment was used to handle dates.
*Code was built from scratch.
*Calendar form to add more reminders does not contains enough validations, I don't have time to add them,
maybe using yup and formik or manually, then it works but you have to capture valid info. (It will be better
if you fill current month info, then you can see in the calendar the reminders that you are adding).
*I only use one city for consuming the weather api but it can be easily extended, I only add London. (time problem again)
*I only mock test saving but if you want to create a more complete unit testing you have to mock store, 
and check if the action was triggered.
*Does not contains multiple month info it works only for the current month.

Thanks and have a great day! =)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.